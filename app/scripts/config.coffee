require.config({
  paths: {
    'vm': 'viewmodels',
    'm': 'models',
    'bower_components': '../bower_components',
    'jquery': '../../bower_components/jquery/dist/jquery',
    'underscore': '../../bower_components/underscore/underscore',
    'localforage': '../../bower_components/localforage/dist/localforage',
    'bootstrap': '../../bower_components/bootstrap/dist/js/bootstrap.js'
  },
  map: {
    '*': {
      'knockout': '../bower_components/knockout/dist/knockout.js',
      'ko': '../bower_components/knockout/dist/knockout.js'
    }
  },
  shim: {
    underscore: {
      exports: '_'
    },
    bootstrap: {deps: ['jquery']}
  }
})

require.config({
  map: {
    '*': {
      'knockout': '../bower_components/knockout/dist/knockout.debug.js',
      'ko': '../bower_components/knockout/dist/knockout.debug.js'
    }
  }
})

#if (!window.requireTestMode) {
#  require(['main'], function(){ });
#}

require(['localforage'], (localforage) ->
  localforage.config({
    driver: localforage.LOCALSTORAGE
  })
)
