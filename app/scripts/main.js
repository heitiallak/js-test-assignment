define(['jquery', 'knockout', 'vm/RecursiveTreePage'], function($, ko, RecursiveTreePage) {
  return ko.applyBindings(new RecursiveTreePage);
});
