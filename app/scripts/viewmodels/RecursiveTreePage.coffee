define ['knockout', 'underscore', 'localforage', 'm/Tree'], (ko, _, lf, Tree) ->
  class RecursiveTreePage
    STORAGE_KEY: 'JS.test.tree'

    constructor: () ->
      @saved = ko.observable(false)
      @fromStore = ko.observable(false)
      @tree = ko.observable( new Tree)
      @pullFromStore()

    pushToStore: (callback) =>
      lf.getItem(@STORAGE_KEY).then (value) =>
        stringified = ko.toJSON(@tree, (key, value) ->
          if key is 'parent' then return undefined else return value)

        lf.setItem(@STORAGE_KEY, stringified)
        @saved(true)
        if callback? then callback()

    pullFromStore: (callback) =>
      stored = lf.getItem(@STORAGE_KEY).then (value) =>
        if value?
          @tree(new Tree(JSON.parse(value).nodes))
          lf.removeItem(@STORAGE_KEY)
          @fromStore(true)

        if callback? then callback()
