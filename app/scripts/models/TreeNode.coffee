define ['knockout', 'underscore'], (ko, _) ->
  class TreeNode
    constructor: (node, @parent) ->
      @name = ko.observable(node.name)
      @nodes = ko.observableArray(
        _.map node.nodes, (node) => return new TreeNode node, @ )
      @hasChildren = ko.computed => @nodes().length > 0
      @addChild = -> @nodes.push(new TreeNode(
        {name: "", nodes: []}, @))
      @remove = -> @parent.nodes.remove @
