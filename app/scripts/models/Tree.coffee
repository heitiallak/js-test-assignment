define ['knockout', 'm/TreeNode', 'underscore'], (ko, TreeNode, _)->
  class Tree
    constructor: (nodes) ->
      transformed = _.map nodes, (node) =>
        return new TreeNode node, @
      @nodes = ko.observableArray(transformed)
      @addChild = => @nodes.push(new TreeNode {name: "", nodes: []}, @)
      @hasChildren = ko.computed => @nodes().length > 0
