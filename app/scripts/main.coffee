# to depend on a bower installed component:
# define(['component/componentName/file'])



define(['jquery', 'knockout', 'vm/RecursiveTreePage'],
  ($, ko, RecursiveTreePage) ->
    ko.applyBindings(new RecursiveTreePage)
)
