define(['chai', 'm/TreeNode'], function(chai, TreeNode) {
  return describe('TreeNode', function() {
    it('should get name from constructor JSON', function() {
      var tn;
      tn = new TreeNode({
        name: "TestName",
        nodes: []
      });
      return chai.expect(tn.name()).to.equal('TestName');
    });
    it('should have children if they have been added', function() {
      var tn;
      tn = new TreeNode({
        name: "TestName",
        nodes: []
      });
      tn.addChild();
      return chai.expect(tn.hasChildren()).to.equal(true);
    });
    it('should get children from constructor JSON', function() {
      var tn;
      tn = new TreeNode({
        name: "TestName",
        nodes: [
          {
            name: "ChildNode1",
            nodes: []
          }, {
            name: "ChildNode2",
            nodes: []
          }
        ]
      });
      chai.expect(tn.nodes().length).to.equal(2);
      return chai.expect(tn.nodes()[0].name()).to.equal('ChildNode1');
    });
    return it('should be able to have multiple levels of children', function() {
      var childNode, tn;
      tn = new TreeNode({
        name: "TestName",
        nodes: [
          {
            name: "ChildNode",
            nodes: [
              {
                name: "DeepChild",
                nodes: []
              }
            ]
          }
        ]
      });
      childNode = tn.nodes()[0];
      chai.expect(childNode.hasChildren()).to.equal(true);
      return chai.expect(childNode.nodes()[0].name()).to.equal("DeepChild");
    });
  });
});
