require.config({
  paths: {
    'vm': '../app/scripts/viewmodels',
    'm': '../app/scripts/models',
    'bower_components': '../bower_components',
    'jquery': '../bower_components/jquery/dist/jquery',
    'underscore': '../bower_components/underscore/underscore',
    'localforage': '../bower_components/localforage/dist/localforage',
    'bootstrap': '../../bower_components/bootstrap/dist/js/bootstrap.js',
    'chai': '../node_modules/chai/chai',
    'sinon': '../node_modules/sinon/lib/sinon'
  },
  map: {
    '*': {
      'knockout': '../bower_components/knockout/dist/knockout.js',
      'ko': '../bower_components/knockout/dist/knockout.js'
    }
  },
  shim: {
    underscore: {
      exports: '_'
    },
    bootstrap: {deps: ['jquery']}
  }
})

require(['TreeTest', 'TreeNodeTest', 'RecursiveTreePageTest'], (t, chai) ->
  if (window.mochaPhantomJS)
    mochaPhantomJS.run()
  else
    mocha.run()

)
