define(['chai', 'localforage', 'vm/RecursiveTreePage', 'sinon'], (chai, lf, RecursiveTreePage, sinon) ->
  describe 'RecursiveTreePage', ->
    beforeEach ->
      @lfMock = sinon.mock(lf)
      @rtp = new RecursiveTreePage

    afterEach ->
      @lfMock.restore()

    it 'should have an empty tree by default', (done)->
      chai.expect(@rtp.tree().hasChildren()).to.equal(false)
      done()

    it 'should be able to save to local storage', (done) ->
      @lfMock.expects('setItem').once();

      @rtp.tree().addChild()
      @rtp.pushToStore( =>
        @lfMock.verify()
        done()
      )

    it 'should be able to load from local storage', (done) ->
      stub = sinon.stub(lf, 'getItem', -> {then: (value)-> return value(JSON.stringify({nodes: [{name: "TestNode", nodes: []}]}))})

      @rtp.pullFromStore(=>
        chai.expect(@rtp.tree().nodes()[0].name()).to.equal("TestNode")
        stub.restore()
        done()
      )

    it 'should try to load from local storage when object is created', ->
      @lfMock.expects('getItem').once()

      @lfMock.verify;
)
