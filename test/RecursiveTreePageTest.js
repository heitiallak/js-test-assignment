define(['chai', 'localforage', 'vm/RecursiveTreePage', 'sinon'], function(chai, lf, RecursiveTreePage, sinon) {
  return describe('RecursiveTreePage', function() {
    beforeEach(function() {
      this.lfMock = sinon.mock(lf);
      return this.rtp = new RecursiveTreePage;
    });
    afterEach(function() {
      return this.lfMock.restore();
    });
    it('should have an empty tree by default', function(done) {
      chai.expect(this.rtp.tree().hasChildren()).to.equal(false);
      return done();
    });
    it('should be able to save to local storage', function(done) {
      this.lfMock.expects('setItem').once();
      this.rtp.tree().addChild();
      return this.rtp.pushToStore((function(_this) {
        return function() {
          _this.lfMock.verify();
          return done();
        };
      })(this));
    });
    it('should be able to load from local storage', function(done) {
      var stub;
      stub = sinon.stub(lf, 'getItem', function() {
        return {
          then: function(value) {
            return value(JSON.stringify({
              nodes: [
                {
                  name: "TestNode",
                  nodes: []
                }
              ]
            }));
          }
        };
      });
      return this.rtp.pullFromStore((function(_this) {
        return function() {
          chai.expect(_this.rtp.tree().nodes()[0].name()).to.equal("TestNode");
          stub.restore();
          return done();
        };
      })(this));
    });
    return it('should try to load from local storage when object is created', function() {
      this.lfMock.expects('getItem').once();
      return this.lfMock.verify;
    });
  });
});
