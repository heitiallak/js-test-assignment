define(['chai', 'm/Tree', 'm/TreeNode'], function(chai, Tree, TreeNode) {
  return describe('Tree', function() {
    beforeEach(function() {
      return this.t = new Tree;
    });
    it('should not have chidlren in a new Tree', function() {
      return chai.expect(this.t.hasChildren()).to.equal(false);
    });
    it('should have children if they have been added', function() {
      this.t.addChild();
      return chai.expect(this.t.hasChildren()).to.equal(true);
    });
    it('should have children of type TreeNode', function() {
      this.t.addChild();
      return chai.expect(this.t.nodes()[0]).to.be.an.instanceOf(TreeNode);
    });
    return it('should be able to contain many children', function() {
      this.t.addChild();
      this.t.addChild();
      return chai.expect(this.t.nodes().length).to.equal(2);
    });
  });
});
