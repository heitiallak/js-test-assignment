define(['chai', 'm/TreeNode'], (chai, TreeNode) ->
  describe 'TreeNode', ->
    it 'should get name from constructor JSON', ->
      tn = new TreeNode {name: "TestName", nodes: []}
      chai.expect(tn.name()).to.equal('TestName')

    it 'should have children if they have been added', ->
      tn = new TreeNode {name: "TestName", nodes: []}
      tn.addChild();
      chai.expect(tn.hasChildren()).to.equal(true)

    it 'should get children from constructor JSON', ->
      tn = new TreeNode {name: "TestName", nodes:
        [{name: "ChildNode1", nodes: []},
        {name: "ChildNode2", nodes: []}]}
      chai.expect(tn.nodes().length).to.equal(2)
      chai.expect(tn.nodes()[0].name()).to.equal('ChildNode1')

    it 'should be able to have multiple levels of children', ->
      tn = new TreeNode {name: "TestName", nodes:
        [{name: "ChildNode", nodes: [{name: "DeepChild", nodes: []}]}]}

      childNode = tn.nodes()[0];
      chai.expect(childNode.hasChildren()).to.equal(true)
      chai.expect(childNode.nodes()[0].name()).to.equal("DeepChild")
)
