define(['chai', 'm/Tree', 'm/TreeNode'], (chai, Tree, TreeNode) ->
  describe 'Tree', ->
    beforeEach ->
      @t = new Tree

    it 'should not have chidlren in a new Tree', ->
      chai.expect(@t.hasChildren()).to.equal(false)

    it 'should have children if they have been added', ->
      @t.addChild()
      chai.expect(@t.hasChildren()).to.equal(true)

    it 'should have children of type TreeNode', ->
      @t.addChild()
      chai.expect(@t.nodes()[0]).to.be.an.instanceOf(TreeNode)

    it 'should be able to contain many children', ->
      @t.addChild()
      @t.addChild()
      chai.expect(@t.nodes().length).to.equal(2)
)
