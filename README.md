Assuming you have node, npm, bower and gulp just cd to project folder and
 
```
npm install && bower install && gulp
```

To run either open `dist/app/index.html` in your browser or run

`gulp serve`
