'use strict';
// generated on 2015-07-21 using generator-knockout-gulp-bootstrap 0.0.2
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

var less = require('gulp-less');
gulp.task('styles', function () {
  return gulp.src('app/styles/main.less')
    .pipe(less())
    .pipe(gulp.dest('dist/app/styles'));
});

var coffeelint = require('gulp-coffeelint');
gulp.task('lint', function () {
  return gulp.src('app/scripts/**/*.coffee')
    .pipe(coffeelint())
    .pipe(coffeelint.reporter());
});

gulp.task('html', ['styles'], function () {
  var lazypipe = require('lazypipe');
  var cssChannel = lazypipe()
    .pipe($.csso)
  var assets = $.useref.assets({searchPath: '{.tmp,app}'});

  return gulp.src('app/*.html')
    //.pipe(assets)
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', cssChannel()))
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe(gulp.dest('dist/app'));
});

gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));

gulp.task('connect', function () {
  var serveStatic = require('serve-static');
  var serveIndex = require('serve-index');
  var app = require('connect')()
    .use(require('connect-livereload')({port: 35729}))
    .use(serveStatic('app'))
    .use(serveStatic('.tmp'))
    // paths to bower_components should be relative to the current file
    // e.g. in app/index.html you should use ../bower_components
    .use('/bower_components', serveStatic('bower_components'))
    .use(serveIndex('app'));

  require('http').createServer(app)
    .listen(9000)
    .on('listening', function () {
      console.log('Started connect web server on http://localhost:9000');
    });
});

gulp.task('serve', ['connect', 'styles'], function () {
  require('opn')('http://localhost:9000');
});

gulp.task('watch', ['connect', 'serve'], function () {
  $.livereload.listen();

  // watch for changes
  gulp.watch([
    'app/*.html',
    'app/styles/**/*.less',
    'app/scripts/**/*.js',
    'app/scripts/**/*.coffee',
  ]).on('change', $.livereload.changed);

  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('bower.json', ['wiredep']);
});

gulp.task('build', ['test', 'coffee', 'lint', 'html', 'bower-src'], function () {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

var mochaPhantomJS = require('gulp-mocha-phantomjs');

gulp.task('test', ['coffee-test'], function () {
  return gulp
    .src('test/index.html')
    .pipe(mochaPhantomJS());
});

var bowerSrc = require('gulp-bower-src')
gulp.task('bower-src', function() {
  bowerSrc().pipe(gulp.dest('dist/bower_components'))
})

var coffee = require('gulp-coffee');
var gutil = require('gulp-util');

gulp.task('coffee', function() {
  gulp.src('./app/scripts/**/*.coffee')
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest('dist/app/scripts/'))
    //.on('error', gutil.log)
});

gulp.task('coffee-test', function() {
  gulp.src('./test/**/*.coffee')
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest('./test/'))
  //.on('error', gutil.log)
});

gulp.task('default', ['clean'], function () {
  gulp.start('build');
});
